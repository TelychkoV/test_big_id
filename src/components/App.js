import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Sidebar from "./Sidebar";
import Overview from './Overview';
import Details from "./Details";
import '../styles/App.css';

const App = () => {
  return (
      <Router>
          <Route render={({ location, history }) => (
              <>
                  <Sidebar
                      location={location}
                      history={history}
                  />
                  <main className='container'>
                      <Route exact path="/overview" component={Overview} />
                      <Route exact path="/details" component={Details} />
                  </main>
              </>
          )}
          />
      </Router>
  );
};

export default App;
