import React from 'react';
import { Form, Button, Row } from 'react-bootstrap';
import { useFetchData, useCustomForm } from "../customHooks";

const Details = () => {
    const [{ data, isLoading, isError }] = useFetchData('https://api.printful.com/countries', [], false);
    const { inputs, handleInputChange, handleSubmit } = useCustomForm();

    if(isError) {
        return <div>Something went wrong ...</div>;
    }

    return (
        <>
            <h1>Details</h1>
            <Form onSubmit={handleSubmit}>
                <Form.Group as={Row}>
                    <Form.Label>Country</Form.Label>
                    <Form.Control
                        size='lg'
                        type="text"
                        as="select"
                        name='country'
                        value={inputs.country}
                        onChange={handleInputChange}>
                        {isLoading ?
                            <option>Loading ...</option> :
                            data.map(({ name }, index) =>
                                <option key={index} value={name}>{name}</option>
                        )}
                    </Form.Control>
                </Form.Group>

                <Form.Group as={Row}>
                    <Form.Label>Count</Form.Label>
                    <Form.Control
                        size='lg'
                        type="number"
                        name='count'
                        value={inputs.count}
                        placeholder="Enter number"
                        onChange={handleInputChange} />
                </Form.Group>
                <Button type="submit" block disabled={!inputs.country || !inputs.count}>
                    Submit
                </Button>
            </Form>

        </>

    )
};

export default Details;