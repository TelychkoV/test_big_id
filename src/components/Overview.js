import React from 'react';
import { useFetchData } from "../customHooks";
import Chart from "react-google-charts";

const Overview = () => {
    const [{ data, isLoading, isError }] = useFetchData('http://localhost:3000/data', [], true);

    if(isError) {
        return <div>Something went wrong ...</div>;
    }

    if(isLoading) {
        return <div>Loading ...</div>;
    }

    return (
        <>
            <h1>Total Users: {data.total}</h1>
            <Chart
                width={'900px'}
                height={'600px'}
                chartType="GeoChart"
                data={data.countries}
                mapsApiKey="AIzaSyDqO7dfLk0-7dDX6IvhL1xD7PcR0_BPekE"
            />
        </>
    )
};

export default Overview;