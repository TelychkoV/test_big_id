import React from 'react';
import SideNav, { NavItem, NavIcon, NavText } from '@trendmicro/react-sidenav';
import '../styles/Sidebar.css';

const Sidebar = ({ location, history }) => {
    return (
        <>
            <SideNav
                onSelect={(selected) => {
                    const to = '/' + selected;
                    if (location.pathname !== to) {
                        history.push(to);
                    }
                }}
            >
                <SideNav.Toggle />
                <SideNav.Nav defaultSelected="overview">
                    <NavItem eventKey="overview">
                        <NavIcon>
                            <i className="fa fa-home" />
                        </NavIcon>
                        <NavText>
                            Overview
                        </NavText>
                    </NavItem>
                    <NavItem eventKey="details">
                        <NavIcon>
                            <i className="fa fa-info" />
                        </NavIcon>
                        <NavText>
                            Details
                        </NavText>
                    </NavItem>
                </SideNav.Nav>
            </SideNav>
        </>
    );
}

export default Sidebar;
