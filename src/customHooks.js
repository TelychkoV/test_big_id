import { useState, useEffect } from 'react';

export const useFetchData = (initialUrl, initialData, transform) => {
    const [data, setData] = useState(initialData);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            setIsError(false);
            setIsLoading(true);
            try {
                const result = await fetch(initialUrl);
                const data = await result.json();

                if(transform) {
                    data.countries = data.countries.reduce((acc, curr) => {
                        acc.push([curr.country, curr.count]);
                        return acc;
                    }, [['Country', 'Popularity']]);
                }

                if('result' in data) {
                    setData(data.result);
                } else {
                    setData(data);
                }

            } catch (error) {
                setIsError(true);
            }
            setIsLoading(false);
        };
        fetchData();
    }, [initialUrl]);
    return [{ data, isLoading, isError }];
};

// Example of form hook (I promised to show you)
export const useCustomForm = () => {
    const [ inputs, setInputs ] = useState({});

    const handleSubmit = (event) => {
        event.preventDefault();

        // Did not make request because it may break server data

        // try {
        //     fetch('http://localhost:3000/data',  {
        //         method: 'POST',
        //         headers: {
        //             'Content-Type': 'application/json'
        //         },
        //         body: JSON.stringify(inputs)
        //     });
        // } catch (error) {}
    };

    const handleInputChange = event => {
        event.persist();
        setInputs(inputs => ({ ...inputs, [event.target.name]: event.target.value }));
    };

    return { handleSubmit, handleInputChange, inputs };
};